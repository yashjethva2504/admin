import { useRouter } from "next/router";
import { Formik } from "formik";

import { AddUser } from "../../../api";
import { FormControl, Select, MenuItem } from "@material-ui/core";
import { toast } from "react-toastify";
import Layout from 'Layouts';
import { Card, CardBody } from '@paljs/ui/Card';
import Col from '@paljs/ui/Col';
import Row from '@paljs/ui/Row';
import { InputGroup } from '@paljs/ui/Input';
import styled from 'styled-components';
import { Button } from '@paljs/ui/Button';
import React, { useState } from 'react';

import * as Yup from "yup";

export const SelectStyled = styled(Select)`
  margin-bottom: 1rem;
`;
const selectstyle = { marginTop: '10px' };
const UserCreate = () => {

    

const validationSchema = Yup.object().shape({
    company: Yup.string()
        .required('Required'),
    email: Yup.string().email('Invalid email address').required('Required'),
    password : Yup.string()
                .required('Required'),
    confirm_password: Yup.string()
                .oneOf([Yup.ref('password'), null], 'Passwords must match')
                .required('Required'),
    maximum_search_allowed: Yup.string().required('Required'),
});


  const [loading, setLoading] = useState(false);
  
  const statusArray = [
    { value: "Active", label: "Active" },
    { value: "InActive", label: "InActive" }
  ];
  
  const router = useRouter();
  return (
    <Layout className="create">
      {
        !loading ?
        (
          <>
            <Row>
              <Col breakPoint={{ xs: 12, md: 12 }}>
                <Card>
                  <header>Create User</header>
                  <CardBody>
                    <Row>
                      <Col breakPoint={{ xs: 12, md: 12 }}>
                        <Formik
                          enableReinitialize="true"
                          initialValues={{
                            company: '',
                            email: '',
                            password: '',
                            maximum_search_allowed:100,
                          }}
                          validationSchema={validationSchema}
                          onSubmit={async (values) => {
                            setLoading(true);
                            await AddUser(values)
                            .then(function(res) {
                              if(res && res.status == 200) {
                                toast.success("User Added Successfully");
                                router.push({
                                  pathname: '/users'
                                });
                              } else {
                                setLoading(false);
                                toast.error(res.data.error);
                                
                              }
                            })
                          }}
                        >
                          {({ handleSubmit, handleChange, values, setFieldValue, errors, touched }) => (
                            <div>
                              <Row>
                                <Col breakPoint={{ xs: 12, md: 6 }}>
                                  <div style={{ marginTop: '10px' }}>
                                    <label htmlFor="name">Company/ Name <span className="required">*</span></label>
                                    <InputGroup fullWidth>
                                      <input id="name" name="company" type="text" onChange={handleChange} value={values.company} className={touched.company && errors.company ? "error" : null} />

                                    </InputGroup>
                                    {touched.company && errors.company ? (
                                      <div className="error-message">{errors.company}</div>
                                    ) : null}
                                  </div>
                                </Col>
                                <Col breakPoint={{ xs: 12, md: 6 }}>
                                  <div style={{ marginTop: '10px' }}>
                                    <label htmlFor="email">Email Id <span className="required">*</span></label>
                                    <InputGroup fullWidth>
                                      <input id="email" name="email" type="text" onChange={handleChange} value={values.email} className={touched.email && errors.email ? "error" : null} />
                                    </InputGroup>
                                    {touched.email && errors.email ? (
                                      <div className="error-message">{errors.email}</div>
                                    ) : null}
                                  </div>
                                </Col>
                                <Col breakPoint={{ xs: 12, md: 6 }}>
                                  <div style={{ marginTop: '10px' }}>
                                    <label htmlFor="password">Password <span className="required">*</span></label>
                                    <InputGroup fullWidth>
                                      <input id="password" name="password" type="password" onChange={handleChange} value={values.password} className={touched.password && errors.password ? "error" : null} />
                                    </InputGroup>
                                    {touched.password && errors.password ? (
                                      <div className="error-message">{errors.password}</div>
                                    ) : null}
                                  </div>
                                </Col>
                                <Col breakPoint={{ xs: 12, md: 6 }}>
                                  <div style={{ marginTop: '10px' }}>
                                    <label htmlFor="confirm_password">Confirm Password <span className="required">*</span></label>
                                    <InputGroup fullWidth>
                                      <input id="confirm_password" name="confirm_password" type="password" onChange={handleChange} value={values.confirm_password} className={touched.confirm_password && errors.confirm_password ? "error" : null} />
                                    </InputGroup>
                                    {touched.confirm_password && errors.confirm_password ? (
                                      <div className="error-message">{errors.confirm_password}</div>
                                    ) : null}
                                  </div>
                                </Col>

                                <Col breakPoint={{ xs: 12, md: 6 }}>
                                  <div style={{ marginTop: '10px' }}>
                                    <label htmlFor="maximum_search_allowed">Maximum Search Allowed <span className="required">*</span></label>
                                    <InputGroup fullWidth>
                                      <input id="maximum_search_allowed" name="maximum_search_allowed" type="number" onChange={handleChange} value={values.maximum_search_allowed} className={touched.maximum_search_allowed && errors.maximum_search_allowed ? "error" : null} />
                                    </InputGroup>
                                    {touched.maximum_search_allowed && errors.maximum_search_allowed ? (
                                      <div className="error-message">{errors.maximum_search_allowed}</div>
                                    ) : null}
                                  </div>
                                </Col>
                                
                              </Row>
                              <Row>
                                <Col breakPoint={{ xs: 12, md: 4 }}></Col>
                                <Col breakPoint={{ xs: 12, md: 6 }}>
                                  <div style={{ marginTop: '10px' }}>
                                    <Button status="Success" type="button" shape="SemiRound" onClick={handleSubmit}>
                                      Submit
                                    </Button>
                                  </div>
                                </Col>
                              </Row>
                            </div>
                          )}
                        </Formik>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </>
        ) : (
          null
        )
      }
    </Layout>
  );
};

export default UserCreate;
