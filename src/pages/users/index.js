import { Button, ButtonLink } from '@paljs/ui/Button';
import { Card, CardBody } from '@paljs/ui/Card';
import Row from '@paljs/ui/Row';
import Col from '@paljs/ui/Col';
import React, { useRef, useEffect, useState } from 'react';
import Layout from 'Layouts';
import MUIDataTable from "mui-datatables";
import { getUsers, deleteUser } from "../../../api";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faPlus, faTrash, faPencilAlt,faKey } from "@fortawesome/free-solid-svg-icons"; // import the icons you need
import { toast } from "react-toastify";
import { useRouter } from "next/router";
const Table = ({ DataList }) => {
    let data1 = [];
    const router = useRouter()
    const userDelete = async (id) => {
        
        await deleteUser(id)
        .then(function(res) {
            if(res && res.status == 200) {
                toast.success(res.message);
                fetchData()
            } else {
                toast.error(res.message);
            }
        })
    }
    let [data, setData] = useState(false);
    const fetchData = async () => {
        
        await getUsers()
        .then(function(res) {
            if(res && res.status == 200) {
                setData(res.data.users)
            } else {
                toast.error(res.message);
            }
        })
        
    }
    useEffect(() => {
        fetchData()

    }, []);
    const columns = data => [

        {
            name: "Action",
            options: {
                filter: true,
                sort: false,
                empty: true,
                download: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    
                    return (
                        <div>
                            
                            <a className="iconcss" title ="Delete User" style={{"cursor":"pointer",marginLeft:"5px"}} onClick={() => {
                                const confirmBox = window.confirm("Do you really want to delete this User?"
                                )
                                if (confirmBox === true) {
                                    userDelete(`${tableMeta.rowData[1]}`)
                                }
                            }}  ><FontAwesomeIcon icon={faTrash}></FontAwesomeIcon></a>
                            <a className="iconcss" title="Edit User Detail" style={{"cursor":"pointer",marginLeft:"5px"}}
                            onClick={() => {
                                router.push({
                                    pathname: '/users/edit',
                                    query: { id: tableMeta.rowData[1] }
                                }); }}
                             ><FontAwesomeIcon icon={faPencilAlt}></FontAwesomeIcon></a>
                            
                        </div>

                    );

                }
            }
        },
        {
            name: "User  Id",
            options: {
                filter: true,
            }
        },
        {
            name: "Name",
            options: {
                filter: true,
            }
        },
        {
            name: "Email Id",
            options: {
                filter: true,
            }
        },
        {
            name: "searches_done",
            options: {
                filter: true,
            }
        },
        {
            name: "maximum_search_allowed",
            options: {
                filter: true,
            }
        }
        

    ];

    const getTableData = data => {


        if (Array.isArray(data) && data.length) {
            for (let row = 0; row < data.length; row++) {
                data1.push([data[row]._id, data[row].company, data[row].email, data[row].searches_done, data[row].maximum_search_allowed]);
            };
        }
        return data1;
    }

    const options = {
        filterType: 'checkbox',
    };
    const style = { marginBottom: '1.5rem' };
    const bstyle = { marginBottom: '1rem' };
    return (

        <Layout title="Button">
            <Row>
                <Col breakPoint={{ xs: 12, lg: 12 }}>
                    <Row>
                        <Col breakPoint={{ xs: 12, lg: 9 }}>
                            <h1>
                                Users
                            </h1>
                        </Col>
                        <Col key="Success" breakPoint={{ xs: 12, lg: 3 }}>
                            <Button style={bstyle} fullWidth appearance="hero" status="Success">
                                <a href={`/users/create`} className="addbutton" style={{"color":"white"}}>
                                    Add New User
                                </a>
                            </Button>
                        </Col>
                       
                    </Row>
                </Col>
            </Row>
            <Row>
                <Col breakPoint={{ xs: 12, lg: 12 }}>
                    <Card>
                        <CardBody>
                            <MUIDataTable
                                title={"Users"}
                                data={getTableData(data)}
                                columns={columns(data)}
                                options={{
                                    downloadOptions: {
                                        filename: "user.csv",
                                        separator: ","
                                    },
                                    selectableRows: false // <===== will turn off checkboxes in rows
                                }}
                            />
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </Layout>
    );
};



export default Table;
