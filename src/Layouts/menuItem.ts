import { MenuItemType } from '@paljs/ui/types';

const items: MenuItemType[] = [
  {
    title: 'Home Page',
    icon: { name: 'home' },
    link: { href: '/' },
  },
  {
    title: 'Users',
    icon: { name: 'people-outline' },
    link: { href: '/users' },
  },
  
];

export default items;
