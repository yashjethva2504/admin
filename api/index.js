import axiosInstance from "../services";

export const login = (data) => {
  return axiosInstance.post(`/admin-login/`,data);
};

export const register = (data) => {
  return axiosInstance.post(`/register/`,data);
};

export const getUsers = async () => {
  const response = axiosInstance.get(
      `/get-users`
  );
  return response;
};
export const deleteUser = async (id) => {
  const response = axiosInstance.delete(
      `/delete-user/${id}`
  );
  return response;
};

export const AddUser = (data) => {
  return axiosInstance.post(`/save-user`, data);
};
export const getUserbyid = async (id) => {
  const response = axiosInstance.get(
      `/get-user/${id}`
  );
  return response;
};
export const UpdateUser = (data, id) => {
  return axiosInstance.post(`/update-user/${id}`, data);
};

